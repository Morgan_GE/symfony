<?php

namespace OC\PlatformBundle\Antispam;

use Twig_Function_Method;

class OCAntispam extends \Twig_Extension
{
    private $mailer;
    private $locale;
    private $nbForSpam;
    
    public function __construct(\Swift_Mailer $mailer, $nbForSpam)
    {
        $this->mailer = $mailer;
        $this->nbForSpam = (int) $nbForSpam;
    }
   
    public function setlocale($locale)
    {
        $this->locale = $locale;
    }
    
    public function getFunctions()
    {
        return array('checkIfSpam' => new \Twig_Function_Method($this, 'isSpam'));
    }
    public function getname()
    {
        return 'OCAntispam';
    }
}

