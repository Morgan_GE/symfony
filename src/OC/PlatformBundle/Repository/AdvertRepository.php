<?php

namespace OC\PlatformBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

class AdvertRepository extends EntityRepository
{
    public function getPublishedQueryBUilder()
    {
        return $this
          ->createQueryBuilder('a')
          ->where('a.published = :published')
          ->setParameter('published', true)
        ;
    }
        public function findByAuthor2($author)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->where('a.author = :author')
             ->setParameter('author', $author);
        return $qb->getQuery()->getResult();
    }
    public function getAdvertWithCategories(array $categoryNames)
    {
        $qb = $this->CreateQueryBuilder('a')->innerJoin('a.category', 'cat')->addSelect('cat');
        
        $qb->where($qb->expr()->in('cat.name', $categoryNames));
        return $qb->getQuery()->getResult();
    }
    public function getApplicationsWithAdvert($limit)
    {
        $qb = $this->CreateQueryBuilder('a')->innerJoin('a.advert', 'adv')->addSelect('adv');
        $qb->setMaxResults($limit);
        
        return $qb->getQuery()->getResult();
    }
    public function getAdverts($page, $nbPerPage)
    {
      $qb = $this->createQueryBuilder('a')
          ->leftJoin('a.image', 'i')
          ->addSelect('i')
          ->leftJoin('a.categories', 'c')
          ->addSelect('c')
          ->orderBy('a.date', 'DESC')
          ->getQuery();
        
        $qb->setFirstResult(($page-1) * $nbPerPage)->setMaxResults($nbPerPage);
        return new Paginator($qb, true);
    }
}

